<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ladingpage extends Model
{
    Protected $table = 'ladingpages';
    protected $fillable = ['name','lastname','email','number','identification','message'];
    protected $guarded = ['id'];
}
