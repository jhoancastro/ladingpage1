<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ladingpage as Ladingpage;

use App\Http\Requests\LadingRequest;

class LadingpageController extends Controller
{
    public function index()
	{
		$lading = Ladingpage::all();
		return \View::make('lading/lading' ,compact('lading'));
	}

	public function store(LadingRequest $request)
	{
		$lading = new Ladingpage;
	    $lading->name = $request->name;
	    $lading->lastname = $request->lastname;
	    $lading->identification = $request->identification;
	    $lading->number = $request->number;
	    $lading->email = $request->email;
	    $lading->message = $request->message;
	    $lading->save();
	    return redirect('lading');
	}
}
