@extends('layouts.app')
@section('content')

	<section class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 mb-5" >
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner">
				    <div class="carousel-item active">
				      <img src="{{ asset('images/image1.jpg') }}" class="d-block w-100" alt="...">
				    </div>
				    <div class="carousel-item">
				      <img src="{{ asset('images/image2.jpg') }}" class="d-block w-100" alt="...">
				    </div>
				    <div class="carousel-item">
				      <img src="{{ asset('images/image3.jpg') }}" class="d-block w-100" alt="...">
				    </div>
				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
			

			<div id="cont-ladin" class="col-md-6 mt-3 mb-3">
				<div class="form-group">
					<h3>Formulario de Registro</h3>
				</div>
				@include('lading.fragment.error')
				{!! Form::open(['route' => 'lading.store', 'method' => 'post', 'novalidate' ]) !!}
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="name" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Apellido</label>
						<input type="text" name="lastname" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Correo</label>
						<input type="email" name="email" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Numero</label>
						<input type="number" name="number" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Identificacion</label>
						<input type="number" name="identification" class="form-control" required>
					</div>
					 <div class="form-group">
					    <label for="message">Mensage</label>
					    <textarea class="form-control" name="message" id="message" rows="3"></textarea>
					  </div>
					<div  id="btnm" class="form-group">
						<button type="subit" class="btn btn-success">Registar</button>
					</div>

				{!! Form::close() !!}
			</div>
		</div>
	</section>

@endsection